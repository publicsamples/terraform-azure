# Terraform Azure Struct Sample
# terraform-module

Esse projeto :
* Esse é um exemplo base para criação de projetos na Azure.
* Projeto visando modularização completa para reuso de código.
* Pensado para criar workspaces nos diferentes ambientes (production, staging e development).
* O uso de Makefile e container evita a instalação dos binários e melhora a automação futura dos pipelines

Para usar o make você precisa:

 - [terraform](https://www.terraform.io/) >= 1.0.0
 - [make](https://www.gnu.org/software/make/)

## Entendendo os arquivos

O arquivo **main.tf** na raiz do projeto descreve a conexão com o provider da azure.

O arquivo **terrafile.tf** chama os módulos que irão criar os recursos. Neste exemplo os módulos são baixados direto do repositório durante o comando init. É necessárip permissão de acesso ao repositório e a chave ssh criada anteriorente e esteja disponível na máquina no diretório $HOME/.ssh.

O arquivo **variables.tf** tem as descrições e tipagem das variáveis que serão carregadas de acordo com o ambiente escolhido.

O arquivo **terrafile.tfvars** dentro dos diferentes diretórios nomeados pelo seu ambiente, possuem as variáveis do respectivo ambiente.

O arquivo **.env.example** deve ser renomeado para .env e conter as chaves necessárias para confirmação das credenciais na Azure. Essas credenciais são exportadas no arquivo makefile.

O backend deve estar no provider para garantir o trabalho em equipe e evitar que os membros do time tenham diferentes states. Caso esteja somente fazendo um teste delete o bloco backend inteiro para que o state fique local.

Exemplo do *main.tf*

```terraform
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.46.0"
    }
  }
  backend "azurerm" {
    resource_group_name  = "iac-resource-group"
    storage_account_name = "storagename"
    container_name       = "containername"
    key                  = "api.terraform.tfstate"
  }
}
provider "azurerm" {
  features {}
}
```
## Usando 
Garanta as permissões necessárias para o Makefile.
```zsh
chmod u+x Makefile
```
#### Comando que mostra um help do comando make
```zsh
make help
```
O arquivo makefile monta os comandos e executa-os dentro do container docker.

#### Para iniciar e recarregar os plugins necessários:
```zsh
make terraform-init 
```
#### Crie a workflow específico para o ambiente. Use um dos comandos abaixo
```zsh
make terraform-new-workspace-dev      ## Cria o workspace de development
make terraform-new-workspace-prod     ## Cria o workspace de production  
make terraform-new-workspace-staging: ## Cria o workspace de staging
```
#### Caso queira mudar para algum workflow, use os comandos abaixo
```zsh
make terraform-set-workspace-dev      ## Seta o workspace para development
make terraform-set-workspace-prod     ## Seta o workspace para producao
make terraform-set-workspace-staging  ## Seta o workspace para staging
```
#### Para conferir em qual workspace esta definido
```zsh
make terraform-workspaces             ## Lista e mostra qual workspace esta setado
```
Caso contrário poderá gerar o plano de um ambiente na workspace de outro ambiente
#### Comando para gerar o plano com as variáveis do ambiente no workspace que esta setado
```zsh
make terraform-plan-dev               ## Executa o terraform plan com variaveis de desenvolvimento e cria o tfplan
make terraform-plan-prod              ## Executa o terraform plan com variaveis de producao e cria o tfplan
make terraform-plan-staging           ## Executa o terraform plan com variaveis de staging e cria o tfplan
```
#### Comando para aplicar o plano no workspace que esta setado
```zsh
make terraform-apply-dev              ## Usa o tfplan.dev para aplicar as mudanças
make terraform-apply-prod             ## Usa o tfplan.prod para aplicar as mudanças
make terraform-apply-staging          ## Usa o tfplan.staging para aplicar as mudanças
```
#### Comando para destruir tudo o que esta no state do workspace setado
```zsh	   
make terraform-destroy-dev            ## Detroi todos os recursos de dev
make terraform-destroy-prod           ## Detroi todos os recursos de prod
make terraform-destroy-staging        ## Detroi todos os recursos de staging
```
#### Para entrar no terminal do container e aplicar outros comandos
```zsh
terraform-sh                          ## terraform console
```

	 
