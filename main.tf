###
### Main - Responsável por criar o provider de produção na azure
### 
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.46.0"
    }
  }
  backend "azurerm" {
    resource_group_name  = "iac-resource-group"
    storage_account_name = "storageterraformuudi"
    container_name       = "apitfstate"
    key                  = "api.terraform.tfstate"
  }
}

provider "azurerm" {
  features {}
}