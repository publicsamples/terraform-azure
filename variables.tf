###
### Variables - Responsável por declaração das variáveis que serão usadas em todos os ambientes
###

# Default environments specs
variable "prefix" {
  type        = string
  description = "The prefix to use for this project"
}
variable "project_name" {
  type        = string
  description = "The name of this project"
}

variable "location" {
  type        = string
  description = "The default region that the project will run"
}

variable "environment" {
  type        = string
  description = "The name of this project"
}