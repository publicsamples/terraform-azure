cnf ?= .env
include $(cnf)
export $(shell sed 's/=.*//' $(cnf))

# Pega a ultima tag
TAG=$(shell git describe --tags --abbrev=0)
GIT_COMMIT=$(shell git log -1 --format=%h)
TERRAFORM_VERSION=1.0.0

# HELP
# Vai mostrar a ajuda em cada tarefa
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

terraform-init: 					## Executa o terraform init para fazer download de todos os plugins necessários
	   docker run --rm -v $$PWD:/app -v $$HOME/.ssh/:/root/.ssh/ -w /app/ -e ARM_SUBSCRIPTION_ID=$$ARM_SUBSCRIPTION_ID -e ARM_CLIENT_ID=$$ARM_CLIENT_ID -e ARM_CLIENT_SECRET=$$ARM_CLIENT_SECRET -e ARM_TENANT_ID=$$ARM_TENANT_ID hashicorp/terraform:$(TERRAFORM_VERSION) init -upgrade=true
	   
terraform-plan-dev:					## Executa o terraform plan com variaveis de desenvolvimento e cria o tfplan
	   docker run --rm -v $$PWD:/app -v $$HOME/.ssh/:/root/.ssh/ -w /app/ -e ARM_SUBSCRIPTION_ID=$$ARM_SUBSCRIPTION_ID -e ARM_CLIENT_ID=$$ARM_CLIENT_ID -e ARM_CLIENT_SECRET=$$ARM_CLIENT_SECRET -e ARM_TENANT_ID=$$ARM_TENANT_ID hashicorp/terraform:$(TERRAFORM_VERSION) plan -var-file=./development/terraform.tfvars -out=tfplan.dev
terraform-plan-prod: 				## Executa o terraform plan com variaveis de producao e cria o tfplan
	   docker run --rm -v $$PWD:/app -v $$HOME/.ssh/:/root/.ssh/ -w /app/ -e ARM_SUBSCRIPTION_ID=$$ARM_SUBSCRIPTION_ID -e ARM_CLIENT_ID=$$ARM_CLIENT_ID -e ARM_CLIENT_SECRET=$$ARM_CLIENT_SECRET -e ARM_TENANT_ID=$$ARM_TENANT_ID hashicorp/terraform:$(TERRAFORM_VERSION) plan -var-file=./production/terraform.tfvars -out=tfplan.prod
terraform-plan-staging: 			## Executa o terraform plan com variaveis de staging e cria o tfplan
	   docker run --rm -v $$PWD:/app -v $$HOME/.ssh/:/root/.ssh/ -w /app/ -e ARM_SUBSCRIPTION_ID=$$ARM_SUBSCRIPTION_ID -e ARM_CLIENT_ID=$$ARM_CLIENT_ID -e ARM_CLIENT_SECRET=$$ARM_CLIENT_SECRET -e ARM_TENANT_ID=$$ARM_TENANT_ID hashicorp/terraform:$(TERRAFORM_VERSION) plan -var-file=./staging/terraform.tfvars -out=tfplan.staging


terraform-apply-dev: 					## Usa o tfplan.dev para aplicar as mudanças
	   docker run --rm -v $$PWD:/app -v $$HOME/.ssh/:/root/.ssh/ -w /app/ -e ARM_SUBSCRIPTION_ID=$$ARM_SUBSCRIPTION_ID -e ARM_CLIENT_ID=$$ARM_CLIENT_ID -e ARM_CLIENT_SECRET=$$ARM_CLIENT_SECRET -e ARM_TENANT_ID=$$ARM_TENANT_ID hashicorp/terraform:$(TERRAFORM_VERSION) apply -auto-approve "tfplan.dev"
terraform-apply-prod: 					## Usa o tfplan.prod para aplicar as mudanças
	   docker run --rm -v $$PWD:/app -v $$HOME/.ssh/:/root/.ssh/ -w /app/ -e ARM_SUBSCRIPTION_ID=$$ARM_SUBSCRIPTION_ID -e ARM_CLIENT_ID=$$ARM_CLIENT_ID -e ARM_CLIENT_SECRET=$$ARM_CLIENT_SECRET -e ARM_TENANT_ID=$$ARM_TENANT_ID hashicorp/terraform:$(TERRAFORM_VERSION) apply -auto-approve "tfplan.prod"
terraform-apply-staging: 					## Usa o tfplan.staging para aplicar as mudanças
	   docker run --rm -v $$PWD:/app -v $$HOME/.ssh/:/root/.ssh/ -w /app/ -e ARM_SUBSCRIPTION_ID=$$ARM_SUBSCRIPTION_ID -e ARM_CLIENT_ID=$$ARM_CLIENT_ID -e ARM_CLIENT_SECRET=$$ARM_CLIENT_SECRET -e ARM_TENANT_ID=$$ARM_TENANT_ID hashicorp/terraform:$(TERRAFORM_VERSION) apply -auto-approve "tfplan.staging"

terraform-workspaces: 		## Seta o workspace para development
	   docker run --rm -v $$PWD:/app -v $$HOME/.ssh/:/root/.ssh/ -w /app/ -e ARM_SUBSCRIPTION_ID=$$ARM_SUBSCRIPTION_ID -e ARM_CLIENT_ID=$$ARM_CLIENT_ID -e ARM_CLIENT_SECRET=$$ARM_CLIENT_SECRET -e ARM_TENANT_ID=$$ARM_TENANT_ID hashicorp/terraform:$(TERRAFORM_VERSION) workspace list

terraform-destroy-dev: 					## Detroi todos os recursos de dev
	   docker run --rm -v $$PWD:/app -v $$HOME/.ssh/:/root/.ssh/ -w /app/ -e ARM_SUBSCRIPTION_ID=$$ARM_SUBSCRIPTION_ID -e ARM_CLIENT_ID=$$ARM_CLIENT_ID -e ARM_CLIENT_SECRET=$$ARM_CLIENT_SECRET -e ARM_TENANT_ID=$$ARM_TENANT_ID hashicorp/terraform:$(TERRAFORM_VERSION) destroy -var-file=./development/terraform.tfvars -auto-approve
terraform-destroy-prod: 					## Detroi todos os recursos de prod
	   docker run --rm -v $$PWD:/app -v $$HOME/.ssh/:/root/.ssh/ -w /app/ -e ARM_SUBSCRIPTION_ID=$$ARM_SUBSCRIPTION_ID -e ARM_CLIENT_ID=$$ARM_CLIENT_ID -e ARM_CLIENT_SECRET=$$ARM_CLIENT_SECRET -e ARM_TENANT_ID=$$ARM_TENANT_ID hashicorp/terraform:$(TERRAFORM_VERSION) destroy -var-file=./production/terraform.tfvars -auto-approve
terraform-destroy-staging: 					## Detroi todos os recursos de staging
	   docker run --rm -v $$PWD:/app -v $$HOME/.ssh/:/root/.ssh/ -w /app/ -e ARM_SUBSCRIPTION_ID=$$ARM_SUBSCRIPTION_ID -e ARM_CLIENT_ID=$$ARM_CLIENT_ID -e ARM_CLIENT_SECRET=$$ARM_CLIENT_SECRET -e ARM_TENANT_ID=$$ARM_TENANT_ID hashicorp/terraform:$(TERRAFORM_VERSION) destroy -var-file=./staging/terraform.tfvars -auto-approve


terraform-set-workspace-dev: 		## Seta o workspace para development
	   docker run --rm -v $$PWD:/app -v $$HOME/.ssh/:/root/.ssh/ -w /app/ -e ARM_SUBSCRIPTION_ID=$$ARM_SUBSCRIPTION_ID -e ARM_CLIENT_ID=$$ARM_CLIENT_ID -e ARM_CLIENT_SECRET=$$ARM_CLIENT_SECRET -e ARM_TENANT_ID=$$ARM_TENANT_ID hashicorp/terraform:$(TERRAFORM_VERSION) workspace select dev
terraform-set-workspace-prod: 		## Seta o workspace para producao
	   docker run --rm -v $$PWD:/app -v $$HOME/.ssh/:/root/.ssh/ -w /app/ -e ARM_SUBSCRIPTION_ID=$$ARM_SUBSCRIPTION_ID -e ARM_CLIENT_ID=$$ARM_CLIENT_ID -e ARM_CLIENT_SECRET=$$ARM_CLIENT_SECRET -e ARM_TENANT_ID=$$ARM_TENANT_ID hashicorp/terraform:$(TERRAFORM_VERSION) workspace select prod
terraform-set-workspace-staging: 	## Seta o workspace para staging
	   docker run --rm -v $$PWD:/app -v $$HOME/.ssh/:/root/.ssh/ -w /app/ -e ARM_SUBSCRIPTION_ID=$$ARM_SUBSCRIPTION_ID -e ARM_CLIENT_ID=$$ARM_CLIENT_ID -e ARM_CLIENT_SECRET=$$ARM_CLIENT_SECRET -e ARM_TENANT_ID=$$ARM_TENANT_ID hashicorp/terraform:$(TERRAFORM_VERSION) workspace select staging


terraform-new-workspace-dev: 		## Cria um workspace de development
	   docker run --rm -v $$PWD:/app -v $$HOME/.ssh/:/root/.ssh/ -w /app/ -e ARM_SUBSCRIPTION_ID=$$ARM_SUBSCRIPTION_ID -e ARM_CLIENT_ID=$$ARM_CLIENT_ID -e ARM_CLIENT_SECRET=$$ARM_CLIENT_SECRET -e ARM_TENANT_ID=$$ARM_TENANT_ID hashicorp/terraform:$(TERRAFORM_VERSION) workspace new dev
terraform-new-workspace-prod: 		## Cria um workspace de staging
	   docker run --rm -v $$PWD:/app -v $$HOME/.ssh/:/root/.ssh/ -w /app/ -e ARM_SUBSCRIPTION_ID=$$ARM_SUBSCRIPTION_ID -e ARM_CLIENT_ID=$$ARM_CLIENT_ID -e ARM_CLIENT_SECRET=$$ARM_CLIENT_SECRET -e ARM_TENANT_ID=$$ARM_TENANT_ID hashicorp/terraform:$(TERRAFORM_VERSION) workspace new prod
terraform-new-workspace-staging: 	## Cria um workspace de producao
	   docker run --rm -v $$PWD:/app -v $$HOME/.ssh/:/root/.ssh/ -w /app/ -e ARM_SUBSCRIPTION_ID=$$ARM_SUBSCRIPTION_ID -e ARM_CLIENT_ID=$$ARM_CLIENT_ID -e ARM_CLIENT_SECRET=$$ARM_CLIENT_SECRET -e ARM_TENANT_ID=$$ARM_TENANT_ID hashicorp/terraform:$(TERRAFORM_VERSION) workspace new staging

terraform-sh: ## terraform console
	  docker run -it --rm -v $$PWD:/app -v $$HOME/.ssh/:/root/.ssh/ -w /app/ -e ARM_SUBSCRIPTION_ID=$$ARM_SUBSCRIPTION_ID -e ARM_CLIENT_ID=$$ARM_CLIENT_ID -e ARM_CLIENT_SECRET=$$ARM_CLIENT_SECRET -e ARM_TENANT_ID=$$ARM_TENANT_ID --entrypoint "" hashicorp/terraform:$(TERRAFORM_VERSION) sh