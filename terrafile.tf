###
### Terrafile - Responsável criar toda a infra necessário chamando os módulos 
###

module "resource_group" {
  source      = "git::ssh://git@gitlab.com/publicsamples/terraform-azure-module.git"
  name        = "${var.prefix}_${var.project_name}_resource_group"
  location    = var.location
  environment = var.environment  
}